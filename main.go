package main

import (
	"database/sql"
	"fmt"
	"github.com/antchfx/htmlquery"
	_ "github.com/go-sql-driver/mysql"
	"golang.org/x/net/html"
	"os"
	"regexp"
	"strings"
	"time"
)

var (
	// Database
	dbUser     = os.Getenv("DATABASE_USER")
	dbPassword = os.Getenv("DATABASE_PASSWORD")
	dbName     = os.Getenv("DATABASE_NAME")
	dbHost     = os.Getenv("DATABASE_HOST")
	dbPort     = os.Getenv("DATABASE_PORT")

	// Crawler config
	requestUrl       = "https://www.imdb.com/search/title/?groups=top_1000&sort=user_rating,desc&count=100&ref_=adv_nxt&start=%d"
	cardXpath        = "//*[@id='main']//div[@class='lister-list']/div[@class='lister-item mode-advanced']/div[@class='lister-item-content']"
	titleXpath       = "//h3/a"
	releaseYearXpath = "//h3/span[2]"
	descriptionXpath = "//p[2]"
	ratingXpath      = "//p/span[@class='certificate']"
	runtimeXpath     = "//p/span[@class='runtime']"
	genreXpath       = "//p/span[@class='genre']"
	directorXpath    = "//p[3]/a"
	pageOffset       = 1
	pageOffsetLimit  = 10
	rgxReleaseYear   = regexp.MustCompile(`\(([0-9]*?)\)`)
	rgxGenre         = regexp.MustCompile(`^\s(\w+),?`)
	rgxRuntime       = regexp.MustCompile(`\d+`)
)

func main() {
	dbConn := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s",
		dbUser,
		dbPassword,
		dbHost,
		dbPort,
		dbName,
	)
	db, err := sql.Open("mysql", dbConn)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	for i := 0; i < pageOffsetLimit; i++ {
		if i != 0 {
			pageOffset += 100
		}
		nextURL := fmt.Sprintf(requestUrl, pageOffset)
		doc, err := htmlquery.LoadURL(nextURL)
		if err != nil {
			fmt.Println(err)
		}
		for _, n := range htmlquery.Find(doc, cardXpath) {
			title := extractInnerText(htmlquery.FindOne(n, titleXpath))
			releaseYear := rgxReleaseYear.FindStringSubmatch(extractInnerText(htmlquery.FindOne(n, releaseYearXpath)))[1]
			description := strings.TrimSpace(extractInnerText(htmlquery.FindOne(n, descriptionXpath)))
			rating := extractInnerText(htmlquery.FindOne(n, ratingXpath))
			runtime := rgxRuntime.FindString(extractInnerText(htmlquery.FindOne(n, runtimeXpath)))
			genre := rgxGenre.FindStringSubmatch(extractInnerText(htmlquery.FindOne(n, genreXpath)))[1]
			director := extractInnerText(htmlquery.FindOne(n, directorXpath))

			func() {
				sql := "INSERT INTO `movies` (`title`, `description`, `rating`, `release`, `runtime`, `genre`, `director`) VALUES (?, ?, ?, ?, ?, ?, ?);"
				stmt, err := db.Prepare(sql)
				if err != nil {
					fmt.Println(err.Error())
				}
				defer stmt.Close()
				_, err = stmt.Exec(
					title,
					description,
					rating,
					releaseYear,
					runtime,
					genre,
					director,
				)
				if err != nil {
					fmt.Println(err.Error())
				}
			}()
		}
		time.Sleep(1 * time.Second)
	}
}

func extractInnerText(n *html.Node) string {
	if n == nil {
		return ""
	}
	return htmlquery.InnerText(n)
}
